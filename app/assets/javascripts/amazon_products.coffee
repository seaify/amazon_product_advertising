$(document).on 'ready page:load', ->

  updateURLParameter = (url, param, paramVal) ->
    newAdditionalURL = ''
    tempArray = url.split('?')
    baseURL = tempArray[0]
    additionalURL = tempArray[1]
    temp = ''
    if additionalURL
      tempArray = additionalURL.split('&')
      i = 0
      while i < tempArray.length
        if tempArray[i].split('=')[0] != param
          newAdditionalURL += temp + tempArray[i]
          temp = '&'
        i++
    rows_txt = temp + '' + param + '=' + paramVal
    baseURL + '?' + newAdditionalURL + rows_txt

  $('#do-search').click ->
    url = updateURLParameter(window.location.href, "keyword", $('#amazon-product-keyword').val())
    url = updateURLParameter(url, "page", 1)
    window.location = url

  $('.link').click ->
    console.log($(this).attr('data'))
    url = updateURLParameter(window.location.href, "keyword", $('#amazon-product-keyword').val())
    url = updateURLParameter(url, "page", $(this).attr('data'))
    window.location = url
