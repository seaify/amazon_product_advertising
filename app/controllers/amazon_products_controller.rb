class AmazonProductsController < ApplicationController

  def index

    @products = []
    @page_numbers = 1
    params[:page] = 1 if params[:page].nil?
    params[:page] = params[:page].to_i

    if params[:keyword].present?
      @products, @page_numbers = products_by_title(params[:keyword], params[:page])
      @products = @products + products_by_isbn(params[:keyword])
    end

    ap @page_numbers

  end

  private
  def products_by_title(title, page)

    response = $request.item_search(
        query: {
            Title: title,
            Sort: 'relevancerank',
            ItemPage: page,
            SearchIndex: 'Books'
        }
    )
    ap response.to_h['ItemSearchResponse']
    if response.to_h['ItemSearchResponse']['Items']['TotalResults'] == '0'
      return [], 0
    else
      return response.to_h['ItemSearchResponse']['Items']['Item'], [10, response.to_h['ItemSearchResponse']['Items']['TotalPages'].to_i].min
    end
  end

  def products_by_isbn(isbn)

    response = $request.item_lookup(
        query: {
            ItemId: isbn
        }
    )
    if response.to_h['ItemLookupResponse']['Items']['Request']['Errors'].present?
      []
    else
      [response.to_h['ItemLookupResponse']['Items']['Item']]
    end

  end


end
